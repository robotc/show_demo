#!/usr/bin/env bash
source /opt/intel/openvino/bin/setupvars.sh
source ~/aikit_ws/devel/setup.bash
gnome-terminal -x bash -c " roslaunch aikit_chassis drive_start.launch;exec bash  "
sleep 2
gnome-terminal -x bash -c " roslaunch aikit_arm claw_service.launch;exec bash"
sleep 2
gnome-terminal -x bash -c "roslaunch aikit_nav aikit_nav_teb_demo.launch map:=empty_map.yaml ;exec bash"
sleep 2
gnome-terminal -x bash -c "roslaunch realsense_camera r200_nodelet_rgbd.launch;exec bash"
sleep 2
gnome-terminal -x bash -c "python ~/Desktop/show_demo/src/nav_claw.py;exec bash"
sleep 3
gnome-terminal -x bash -c "roslaunch ros_openvino myriad_realsense_r200.launch;exec bash"



