#!/usr/bin/env bash
source /opt/intel/openvino/bin/setupvars.sh
source ~/aikit_ws/devel/setup.bash;
gnome-terminal -x bash -c "roslaunch realsense_camera r200_nodelet_rgbd.launch;exec bash"
sleep 3
gnome-terminal -x bash -c "python ~/Desktop/show_demo/src/object_openvino_ros.py;exec bash"
sleep 3
gnome-terminal -x bash -c "python ~/Desktop/show_demo/src/tts_ros.py;exec bash"
sleep 3
gnome-terminal -x bash -c "roslaunch ros_openvino myriad_realsense_r200.launch;exec bash "



