from PyQt5.QtWidgets import QApplication,QMessageBox
from PyQt5 import QtWidgets
#from PyQt5.QtMultimedia import QSound
from Ui_tts import Ui_Form
#from PyQt5.QtGui import QIcon,QPalette,QBrush,QPixmap
import sys,os
from aip import AipSpeech 
class DemoMain(QtWidgets.QMainWindow,Ui_Form):
    def __init__(self):
        super().__init__()
        self.setupUi(self)  #调用Ui_Mainwindow中的函数setupUi实现显示界面
        self.setWindowTitle("在线语音合成")
        # self.setWindowIcon(QIcon("tts.jpg"))
        # window_pale = QPalette()
        # window_pale.setBrush(self.backgroundRole(), QBrush(QPixmap("tts.jpg")))
        # self.setPalette(window_pale)
        self.APP_ID = '11571096'  
        self.API_KEY = 'dD3pVVfIpBciC9IG7W3VbqNr'
        self.SECRET_KEY = 'dBr9fu5lVrjkmOQl1VpOKDlXiHGqKaTW'
        self.client = AipSpeech(self.APP_ID,self.API_KEY,self.SECRET_KEY) 
        self.pushButton.clicked.connect(self.speak)
    def speak(self):
        words=self.plainTextEdit.toPlainText()
        #print(words)
        speed=self.spd.currentText()
        #print(speed)
        pit=self.pit.currentText()
        #print(pit)
        volume=self.vol.currentText()
        #print(volume)
        per_dict={"女1":0,"男1":1,"男2":3,"女2":4}
        per_name=self.per.currentText()
        person=per_dict[per_name]
        #print(person)
        try:
            result= self.client.synthesis(words, 'zh', 1, {
            'spd': speed,  #语速 0最小 9最大
            'pit': pit,    #音调 0最小 9最大   
            'vol': volume, #音量 0最小 15最大
            'per': person  # 发音人 0：女声 1：男声 3:度逍遥 4：度丫丫
            })
            if not isinstance(result, dict): #如果返回正确
                if len(words)>15:
                    file_name=words[0:15]+".mp3"
                else:
                    file_name=words+".mp3"
                with open(file_name, 'wb') as f:  #打开文件，如果没有文件，则创建
                    f.write(result) #写入文件
                # sound = AudioSegment.from_mp3('tts.mp3')
                # sound.export("tts.wav", format="wav")
                # qs = QSound('tts.wav', self)
                # qs.play()
                os.system("play "+file_name)
        except Exception as e:
            print(e)
            QMessageBox.warning(self, '错误', '请检查网络,查看终端') 
if __name__=='__main__':
    app=QApplication(sys.argv)
    demo=DemoMain()
    demo.show()
    sys.exit(app.exec_())
