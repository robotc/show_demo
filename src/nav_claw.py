#!/usr/bin/env python 
# -*- coding: utf-8 -*-
import rospy #导入rospy库
import actionlib #导入actionlib 库
import os,inspect #导入os库
from actionlib_msgs.msg import * #导入actionlib的所有模块
from geometry_msgs.msg import Pose, Point, Quaternion, Twist #导入四个消息数据类型，姿态，目标点，四元数，运动消息Twist
from move_base_msgs.msg import MoveBaseAction, MoveBaseGoal #导入movebase的两个消息数据类型
from tf.transformations import quaternion_from_euler #导入tf变换库的欧拉角转四元数库
from aikit_arm.srv import aikit_claw_srv
from math import pi #导入圆周率pi
from std_msgs.msg import String #导入标准消息的字符串消息数据格式
from ros_openvino.msg import Objects
import random


result=""
object_data=""
object_data_list=["bottle","pottedplant", "cat"]

count=0
last_object=''

#初始化节点
rospy.init_node('nav_claw',anonymous=False)
square_size = 1.0
cmd_vel_pub = rospy.Publisher('cmd_vel', Twist, queue_size=5) #实例化一个消息发布函数
move_base = actionlib.SimpleActionClient("move_base", MoveBaseAction) #action服务器连接

#设置参数 
rospy.loginfo('等待move_base action服务器连接...')
move_base.wait_for_server(rospy.Duration(30))
rospy.loginfo('已连接导航服务')

rospy.wait_for_service("aikit_claw")
rospy.loginfo('已连接机械臂控制服务')
claw = rospy.ServiceProxy("aikit_claw", aikit_claw_srv)

#添加导航坐标点,输入x（前）坐标，y（左）坐标，th（平面朝向0～360度）
def nav_find(x,y,th):
    goal = MoveBaseGoal()
    goal.target_pose.header.frame_id='map'
    goal.target_pose.header.stamp = rospy.Time.now()
    goal.target_pose.pose = pose_e(x,y,th)
    move_base.send_goal(goal)
    while True:
        if object_data in object_data_list:
            print(object_data)
            move_base.cancel_goal()
            return object_data
        state = move_base.get_state()
        if state == GoalStatus.SUCCEEDED:
            rospy.loginfo('随机导航搜索成功')
            return "not_find"

def nav_to(x,y,th):
    goal = MoveBaseGoal()
    goal.target_pose.header.frame_id='map'
    goal.target_pose.header.stamp = rospy.Time.now()
    goal.target_pose.pose = pose_e(x,y,th)
    move_base.send_goal(goal)
    finished_within_time = move_base.wait_for_result(rospy.Duration(20))
    if not finished_within_time:
        move_base.cancel_goal()
        rospy.loginfo('时间超时，导航任务取消。')
    state = move_base.get_state()
    if state == GoalStatus.SUCCEEDED:
		rospy.loginfo('导航成功！')
def shutdown():
    rospy.loginfo('机器人任务停止')
    #rospy.sleep(2)
    cmd_vel_pub.publish(Twist())
    
def pose_e(x,y,th):#输入x（前）坐标，y（左）坐标，th（平面朝向0～360度）
    new_pose=Pose()
    new_pose.position.x=float(x)
    new_pose.position.y=float(y)
    #机器朝向，平面朝向弧度转化成四元数空间位姿
    q=quaternion_from_euler(0.0,0.0,float(th)/180.0*pi)
    new_pose.orientation.x=q[0]
    new_pose.orientation.y=q[1]
    new_pose.orientation.z=q[2]
    new_pose.orientation.w=q[3]
    return  new_pose

def play(filepath):
    cmd="play ~/Desktop/show_demo/mp3/"+filepath+".mp3"
    os.system(cmd)
   
def msg_sub():
    rospy.Subscriber("/object_detection/results", Objects, object_callback, queue_size=1)

def take_cube():
    claw(10)
    rospy.sleep(0.5)
    claw(80)
    rospy.sleep(1)
    play("完成抓取")

def put_cube():
    claw(30)
    play("任务完成")

def clear_data():
    global object_data
    object_data=""

def object_callback(data):
    global count
    global last_object
    global object_data
    data = data.objects
    if data:	        
        for i in data:		
            p = i.confidence
            object_name = i.label
            if p >= 0.70:
                new_object=object_name
                if new_object==last_object:
                    count+=1				
                if count>=7:
                    if object_name != "person" and object_name != "tvmonitor":
                        object_data=object_name
                        #print(object_data)
                        count=0
                last_object=new_object	
def msg_sub():
    rospy.Subscriber("/object_detection/results", Objects, object_callback, queue_size=1)		
    	
if __name__ == "__main__":
    msg_sub()
    claw(30)
    play("start")
    while True:
        clear_data()
        x=random.uniform(0.8,2)
        y=random.uniform(-1,1)
        t=random.randint(-360,360)
        result=nav_find(x,y,t)
        if result in object_data_list:
            shutdown()
            play("发现目标")
            play(result)
            take_cube() 
            clear_data()
            nav_to(0,0,-180)
            put_cube()
            result=""
    rospy.spin()
    



       

        







