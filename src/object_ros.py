#!/usr/bin/env python
# -*- coding: utf-8 -*-
import rospy,os
from std_msgs.msg import String
from object_msgs.msg import ObjectsInBoxes
from std_msgs.msg import String

rospy.init_node('wrc_objects', anonymous=False)
spub=rospy.Publisher("tts_topic",String,queue_size=1)
objdata=String()

def callback(data):
    data = data.objects_vector
    if data:        
        for i in data:
            p = i.object.probability
            object_name = i.object.object_name
            if p >= 0.700:
				spub.publish(object_name)

                 
def object_search():
    rospy.Subscriber("/movidius_ncs_nodelet/detected_objects_multiple", ObjectsInBoxes, callback, queue_size=1)
    rospy.spin()
if __name__ == '__main__':
    print("物体识别语音播报")
    try:
        object_search()
    except rospy.ROSInterruptException:
        rospy.loginfo("object node terminated.")
    
