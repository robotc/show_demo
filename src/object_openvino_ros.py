#!/usr/bin/env python
# -*- coding: utf-8 -*-
import rospy,os
from std_msgs.msg import String
from ros_openvino.msg import Objects
rospy.init_node('wrc_objects', anonymous=False)
spub=rospy.Publisher("tts_topic",String,queue_size=1)
objdata=String()

count=0
last_object=''

def callback(data):
	global count
	global last_object
	data = data.objects
	if data:	        
		for i in data:		
			p = i.confidence
			object_name = i.label
			if p >= 0.70:
				new_object=object_name
				if new_object==last_object:
					count+=1
				
				if count>=7:
					if object_name != "person" and object_name != "tvmonitor":
						spub.publish(object_name)
					count=0

				last_object=new_object			
    
def object_search():
	rospy.Subscriber("/object_detection/results", Objects, callback, queue_size=1)
	rospy.spin()
if __name__ == '__main__':
	print("物体识别语音播报")
	try:
		object_search()
	except rospy.ROSInterruptException:
		rospy.loginfo("object node terminated.")
    
